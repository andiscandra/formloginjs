/*
SQLyog Ultimate v10.42 
MySQL - 5.5.29 : Database - user
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`user` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `user`;

/*Table structure for table `master_data` */

DROP TABLE IF EXISTS `master_data`;

CREATE TABLE `master_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `score` int(3) NOT NULL,
  `emotion` varchar(10) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `master_data` */

insert  into `master_data`(`id`,`nama`,`score`,`emotion`,`created`) values (1,'kevin',80,'happy','2020-02-20'),(2,'josh',90,'sad','2020-02-20'),(3,'kevin',85,'happy','2020-02-20'),(4,'kevin',75,'sad','2020-02-20'),(5,'josh',65,'angry','2020-02-20'),(6,'david',85,'happy','2020-02-21'),(7,'josh',90,'sad','2020-02-21'),(8,'david',75,'sad','2020-02-21'),(9,'josh',85,'sad','2020-02-21'),(10,'josh',70,'happy','2020-02-21'),(11,'kevin',80,'sad','2020-02-21'),(12,'kevin',73,'sad','2020-02-22'),(13,'kevin',75,'angry','2020-02-22'),(14,'david',82,'sad','2020-02-22'),(15,'david',65,'sad','2020-02-22');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(50) NOT NULL,
  `user_pass` varchar(32) NOT NULL,
  `user_fname` varchar(50) NOT NULL,
  `user_age` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`user_email`,`user_pass`,`user_fname`,`user_age`) values (1,'admin@tst.com','b6586f44ee6b274dc72c194a3bf01a0d','admin',20),(2,'admin2@tst.com','979df77511a68e026844038293eb3699','admin 2',23);

/* Procedure structure for procedure `getLogin` */

/*!50003 DROP PROCEDURE IF EXISTS  `getLogin` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `getLogin`(
        userEmail VARCHAR(50),
        userPass VARCHAR(32)
    )
BEGIN
	SELECT * FROM users WHERE user_email = userEmail AND user_pass = MD5(userPass);
    END */$$
DELIMITER ;

/* Procedure structure for procedure `getUsers` */

/*!50003 DROP PROCEDURE IF EXISTS  `getUsers` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `getUsers`()
BEGIN
	SELECT * FROM users;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `insertUsers` */

/*!50003 DROP PROCEDURE IF EXISTS  `insertUsers` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUsers`(
	userEmail VARCHAR(50),
        userPass VARCHAR(32),
        userFname VARCHAR(50),
        userAge int(11)
    )
BEGIN
	INSERT INTO users VALUES(NULL, userEmail, MD5(userPass), userFname, userAge);
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
