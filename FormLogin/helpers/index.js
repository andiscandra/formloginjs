function validateEmail(email) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

function checkPassword(password)
{
    var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    return re.test(password);
}

function loginChecker(req, res, next) {
  if (req.session.authorised) {
    res.redirect('/');
    return;
  } else {
    next();
    return;
  }
}

function checkForm(fields) {

  for (var i = 0; i < fields.length; i++) {
    var currElement = fields[i];

    if (currElement == undefined || currElement == '') {
      return false;
    }

  }
  return true;
}

module.exports.checkForm = checkForm;

module.exports.loginChecker = loginChecker;

module.exports.validateEmail = validateEmail;

module.exports.checkPassword = checkPassword;